;;;; -*- lexical-binding: t -*-

;;;; J. Jakes-Schauer

(cl-defmacro defparameter (&rest args)
  "Alias."
  `(setq ,@args))

(cl-defmacro let-1 (var val &rest body)
  "Shorthand."
  `(let ((,var ,val))
     ,@body))

(cl-defmacro λ (parm-or-parmlist &rest body)
  "Alias for (lambda).  If only one variable is needed, it needn't be parenthesized; e.g.:
  (λ x (* 2 x)) "
  `(lambda ,(etypecase parm-or-parmlist
	      (list parm-or-parmlist)
	      (symbol (list parm-or-parmlist)))
     ,@body))

(cl-defmacro the (type expr)
  "My own implementation of CL's (the), since (cl-the) doesn't do squat."
  (let-1 varname (cl-gensym "evald")
	 `(let-1 ,varname ,expr
		 (progn (check-type ,varname ,type)
			,varname))))

(cl-defmacro with-interactive-print (form)
  "Copied from my Webcourses grading project, with slight modification.  Returns the result of evaluating 'form', and, if the surrounding call is interactive, prints it to stdout as well.
  TODO: Make variadic."
  (let-1 varname (cl-gensym "form")
	 `(let-1 ,varname ,form
		 ;; Here is the reason this has to be a macro:
		 (when (called-interactively-p 'any)
		   (print ,varname))
		 ,varname)))

(cl-defun cygpath (&optional file-or-buffer)
  "Converts a Unix path to a Windows one.  By default, operates on current file."
  (interactive)
  (with-interactive-print
   (let ((file-or-buffer (or file-or-buffer
			     (buffer-file-name))))
     (cl-check-type file-or-buffer string)
     (let ((unix-path (expand-file-name file-or-buffer)))
       (cl-check-type unix-path string)
       (with-temp-buffer
	 (call-process "cygpath" nil t nil "-w" "-a" "-l" "-m" unix-path)
	 (buffer-substring (point-min) (1- (point-max))))))))

;;;; id=SLIME
(cl-defun slime:insert-constructor (&key
                                    (class 'CLASS)
                                    (instance 'x)
                                    (qualifier :after)
                                    (rest '_ rest-provided?))
  (interactive)
  ;; TODO: Print carriage return in second line.
  (let ((ctor `(defmethod initialize-instance ,@(when qualifier (list qualifier)) ((,instance ,class) &rest ,rest &key &allow-other-keys)
                 ,@(unless rest-provided? `((declare (ignore ,rest)))))))
    (insert (format "%s" ctor))))
